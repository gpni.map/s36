
const express = require('express');

const TaskController = require('../controllers/TaskController.js')


const router = express.Router()




router.post('/create', (req, res) =>{
	TaskController.createTask(req.body).then((resultFromController) => res.send(resultFromController))
})



router.get('/:id', (req, res) => {
	TaskController.getTaskById(req.params.id).then((resultFromController) => res.send(resultFromController))
});



router.put('/:id/complete', (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
});



module.exports = router