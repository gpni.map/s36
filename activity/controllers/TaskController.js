
const Task = require('../models/Task.js');





module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})
	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			return savedTask //'Task created successfully!'
		}
		
	})
}	




module.exports.getTaskById = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return false
		} else {
			return result
		}
	})
};




module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			result.status = newContent.status

			return result.save().then((updatedTask, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return updatedTask
				}
			})
		}
	})
}

