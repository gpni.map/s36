// Controllers
/*
- Controllers contain the functions and business logic of our Express JS application
- Meaning all the operations it can do will be placed in this file
*/


// Uses the "require" directive to allow access to the "Task" model which allows us to access Mongoose methods to perform CRUD functions
// Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require('../models/Task.js');


// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions
module.exports.getAllTasks = () => {
	// business logic
	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => {
		// The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result
	})
};


module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})
	return newTask.save().then((savedTask, error) => {
	
		if(error) {
			console.log(error)
			return false
		} else if (savedTask != null && savedTask.name == reqBody.name) {
			return 'Duplicate Task Found!'
		} else {
			return savedTask //'Task created successfully!'
		}
		
	})
}	

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			result.name = newContent.name

			return result.save().then((updatedTask, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return updatedTask
				}
			})
		}
	})
}




module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			return "Task deleted successfully!!" //deletedTask
		}
	})
}