// Express server setup
const express = require('express');
const mongoose = require('mongoose');


// This allows us to use all the routes defined in the "taskRoute.js"
const taskRoutes = require('./routes/taskRoutes.js')

const app = express();

const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect(`mongodb+srv://gperey:admin123@zuitt-batch197.jvr3p9k.mongodb.net/s36?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));

// MIDDLEWARE
// http://localhost:3001/tasks/(URI)
// Assigns an endpoint for every database collections
app.use('/tasks', taskRoutes)



app.listen(port, () => {console.log(`Server is running at port ${port}`)});